FROM gentoo/portage:latest as portage
FROM gentoo/stage3:latest

COPY --from=gentoo/portage:latest /var/db/repos/gentoo /var/db/repos/gentoo

RUN emerge --sync > /dev/null
RUN emerge app-eselect/eselect-repository dev-vcs/git
RUN eselect repository enable bitcoin
RUN emerge --sync
RUN eselect repository create local
RUN mkdir -p /var/db/repos/local/sci-electronics/ghdl/files
ADD ghdl-4.0.0_pre20231218.ebuild /var/db/repos/local/sci-electronics/ghdl
ADD ghdl-4.0.0_pre20231218-no-pyunit.patch /var/db/repos/local/sci-electronics/ghdl/files
RUN cd /var/db/repos/local/sci-electronics/ghdl && \
    ebuild ghdl-4.0.0_pre20231218.ebuild manifest
RUN mkdir /etc/portage/package.accept_keywords || /bin/true
RUN mkdir /etc/portage/package.use || /bin/true
RUN echo "=sys-libs/libbacktrace-1.0_p20230329 ~amd64" > /etc/portage/package.accept_keywords/ghdl.accept_keywords
RUN echo "sci-electronics/ghdl llvm backtrace" > /etc/portage/package.use/ghdl.use
RUN echo "sys-libs/libbacktrace static-libs" >> /etc/portage/package.use/ghdl.use
RUN echo 'FEATURES="nostrip"' >> /etc/portage/make.conf
RUN emerge sci-electronics/ghdl::local sys-libs/libbacktrace > /dev/null
